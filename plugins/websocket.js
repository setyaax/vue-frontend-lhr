import io from "socket.io-client";
// http://server-lhr.multec-api.com:8081/socket.io/
// http://192.168.18.11:8081/socket.io/
const baseURL = process.env.NODE_ENV !== 'production' ? 'https://server-lhr.multec-api.com/' : 'https://server-lhr.multec-api.com/'
const socket = io(baseURL, { 
    autoConnect: true,
    forceNew: true,
    transports: ["websocket"]
  })

export default socket