import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import {
    BCollapse,
    BToast,
    BSpinner,
    BModal,
    BPagination,
} from 'bootstrap-vue'


import 'bootstrap-vue/dist/bootstrap-vue.css'
import "bootstrap/dist/css/bootstrap.css";

Vue.component('b-collapse', BCollapse)
Vue.component('b-toast', BToast)
Vue.component('b-pagination', BPagination)
Vue.component('b-modal', BModal)
Vue.component('b-spinner', BSpinner)
Vue.use(BootstrapVue)