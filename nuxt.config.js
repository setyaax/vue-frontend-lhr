export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'lhr',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
      { 'http-equiv': 'Content-Security-Policy', content: 'upgrade-insecure-requests'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  server: {
    host: '0.0.0.0'
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/bootstrap.js',
    '~/plugins/chartkit.js',
    '~/plugins/websocket.js',
    '~/plugins/chartkit.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth-next',
    '~/modules/ws',
    'bootstrap-vue/nuxt',
  ],

  bootstrapVue: {
    bootstrapCSS: true,
    bootstrapVueCSS: true,
    bootstrapVueSCSS: false
  },

  auth: {
    // Options
    strategies: {
      local: {
        token: {
          required: false,
          type: false
        },
        endpoints: {
          login: { url: 'admin/login', method: 'post', headers: {Authorization: 'Basic bXVsdGVjOm11bHQzYw=='}, propertyName: 'data' },
          user: false,
          logout: false
        }
      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.NODE_ENV !== 'production' ? 'https://server-lhr.multec-api.com/' : 'https://server-lhr.multec-api.com/',
  },

  proxy: [
    'https://stream.multec-client.com/live/**.m3u8',
    ['https://103.87.16.55:1935/streamer/**.m3u8',{ secure:false }]
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
